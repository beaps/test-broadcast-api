const bc = new BroadcastChannel('test');

const SHOW_VIDEO = 'show-video';
const HIDE_VIDEO = 'hide-video';

const dataBroadcastList = document.querySelectorAll("[data-broadcast]");
const targetElement = document.querySelector("video");

function checkAction(action, element) {
	switch (action) {
		case SHOW_VIDEO:
			element.src = projectsData[0].videos[0];
			element.classList.add(SHOW_VIDEO);
			break;
		case HIDE_VIDEO:
			if (element.classList.contains(SHOW_VIDEO)) {
				element.classList.remove(SHOW_VIDEO);
			}
			break;

		default:
			break;
	}
}

if (dataBroadcastList.length > 0) {
	dataBroadcastList.forEach(element => {
		element.addEventListener("click", e => {
			bc.postMessage(e.target.dataset.broadcast);
		});
	});
}

bc.addEventListener('message', e => {
	checkAction(e.data, targetElement);
});